/*****************************************************************************
 *
 * Copyright (C) 2020  Wilhelm Hagemeister <hm@igh.de>
 *
 * This file is part of the data logging service (DLS).
 *
 * DLS is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * DLS is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with DLS. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "MainWindow.h"

/****************************************************************************/

MainWindow::MainWindow(QWidget *parent):
    QMainWindow(parent)
{
  setWindowState(Qt::WindowMaximized);
  setCentralWidget(&dlsGraph);
    //DLS-Graph
  dlsGraph.setDropModel(&dlsModel);
  //FIXME: use an existing file here; generated most likely from the
  //full fledged dlsgui application
  dlsGraph.load("./demo.dlsv",&dlsModel);
}

/****************************************************************************/

MainWindow::~MainWindow()
{
}
/****************************************************************************/
