<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>DLS::ExportDialog</name>
    <message>
        <location filename="ExportDialog.cpp" line="110"/>
        <source>Failed to create export directory %1.</source>
        <translation>Anlegen des Exportverzeichnisses %1 fahlgeschlagen.</translation>
    </message>
    <message>
        <location filename="ExportDialog.cpp" line="122"/>
        <source>Failed to open %1.</source>
        <translation>Anlegen von %1 fehlgeschlagen.</translation>
    </message>
    <message>
        <location filename="ExportDialog.cpp" line="263"/>
        <source>Target Directory</source>
        <translation>Zielverzeichnis</translation>
    </message>
</context>
<context>
    <name>DLS::FilterDialog</name>
    <message>
        <location filename="FilterDialog.cpp" line="83"/>
        <source>At %1: %2</source>
        <translation>An Zeichen %1: %2</translation>
    </message>
    <message>
        <location filename="FilterDialog.cpp" line="100"/>
        <source>Pattern valid.</source>
        <translation>Muster ist gültig.</translation>
    </message>
</context>
<context>
    <name>DLS::Graph</name>
    <message>
        <location filename="Graph.cpp" line="1721"/>
        <source>&amp;Fix measuring line</source>
        <translation>Messlinie &amp;fixieren</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1723"/>
        <source>Fix the measuring line at the current time.</source>
        <translation>Hält die Messlinie an der aktuellen Zeit fest.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1725"/>
        <source>&amp;Remove measuring line</source>
        <translation>Messlinie &amp;entfernen</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1726"/>
        <source>Remove the measuring line.</source>
        <translation>Entfernt die Messlinie.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1728"/>
        <source>&amp;Previous view</source>
        <translation>&amp;Vorherige Ansicht</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1729"/>
        <source>Return to previous view.</source>
        <translation>Kehrt zur vorherigen Ansicht zurück.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1731"/>
        <source>&amp;Next view</source>
        <translation>&amp;Nächste Ansicht</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1732"/>
        <source>Proceed to next view.</source>
        <translation>Wechselt zur nächstspäteren Ansicht.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1734"/>
        <source>&amp;Update</source>
        <translation>&amp;Akt&amp;ualisieren</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1735"/>
        <source>Update displayed data.</source>
        <translation>Lädt die Daten für den aktuellen Zeitbereich neu.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1737"/>
        <source>&amp;Zoom</source>
        <translation>&amp;Zoom</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1738"/>
        <source>Set mouse interaction to zooming.</source>
        <translation>Setzt die Mausinteraktion auf Zoom.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1740"/>
        <source>&amp;Pan</source>
        <translation>&amp;Verschieben</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1741"/>
        <source>Set mouse interaction to panning.</source>
        <translation>Setzt die Mausinteraktion auf Verschieben.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1743"/>
        <source>&amp;Measure</source>
        <translation>&amp;Messen</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1744"/>
        <source>Set mouse interaction to measuring.</source>
        <translation>Setzt die Mausinteraktion auf Messen.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1746"/>
        <source>Zoom in</source>
        <translation>Hereinzoomen</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1747"/>
        <source>Zoom the current view in to half of the time around the center.</source>
        <translation>Stellt den halben Zeitbereich um die Mitte dar.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1750"/>
        <source>Zoom out</source>
        <translation>Herauszoomen</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1751"/>
        <source>Zoom the current view out the double time around the center.</source>
        <translation>Stellt den doppelten Zeitbereich um die Mitte dar.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1754"/>
        <source>Auto range</source>
        <translation>Gesamten Zeitbereich anzeigen</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1756"/>
        <source>Automatically zoom to the data extent.</source>
        <translation>Automatisch auf den maximalen Zeitbereich zoomen.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1760"/>
        <source>Choose date...</source>
        <translation>Datum wählen...</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1762"/>
        <source>Open a dialog for date picking.</source>
        <translation>Öffnet den Dialog zur Datumswahl.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1764"/>
        <source>Today</source>
        <translation>Heute</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1766"/>
        <source>Set the date range to today.</source>
        <translation>Setzt den Zeitbereich auf heute.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1768"/>
        <source>Yesterday</source>
        <translation>Gestern</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1770"/>
        <source>Set the date range to yesterday.</source>
        <translation>Setzt den Zeitbereich auf gestern.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1772"/>
        <source>This week</source>
        <translation>Diese Woche</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1774"/>
        <source>Set the date range to this week.</source>
        <translation>Setzt den Zeitbereich auf diese Woche.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1776"/>
        <source>Last week</source>
        <translation>Letzte Woche</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1778"/>
        <source>Set the date range to last week.</source>
        <translation>Setzt den Zeitbereich auf letzte Woche.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1780"/>
        <source>This month</source>
        <translation>Dieser Monat</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1782"/>
        <source>Set the date range to this month.</source>
        <translation>Setzt den Zeitbereich auf diesen Monat.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1784"/>
        <source>Last month</source>
        <translation>Letzter Monat</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1786"/>
        <source>Set the date range to last month.</source>
        <translation>Setzt den Zeitbereich auf letzten Monat.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1788"/>
        <source>This year</source>
        <translation>Dieses Jahr</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1790"/>
        <source>Set the date range to this year.</source>
        <translation>Setzt den Zeitbereich auf dieses Jahr.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1792"/>
        <source>Last year</source>
        <translation>Letztes Jahr</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1794"/>
        <source>Set the date range to last year.</source>
        <translation>Setzt den zeitbereich auf letztes Jahr.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1796"/>
        <source>Section properties...</source>
        <translation>Abschnittseigenschaften...</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1797"/>
        <source>Open the section configuration dialog.</source>
        <translation>Öffnet den Einstellungsdialog für den Abschnitt.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1800"/>
        <source>Remove section</source>
        <translation>Abschnitt entfernen</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1801"/>
        <source>Remove the selected section.</source>
        <translation>Löscht den gewählten Abschnitt.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1803"/>
        <source>Clear sections</source>
        <translation>Alle Abschnitte entfernen</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1804"/>
        <source>Remove all sections.</source>
        <translation>Löscht alle Abschnitte.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1806"/>
        <source>Show Messages</source>
        <translation>Meldungen zeigen</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1807"/>
        <source>Show process messages.</source>
        <translation>Stellt Meldungen des Prozesses dar.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1809"/>
        <source>Filter messages...</source>
        <translation>Meldungen filtern...</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1810"/>
        <source>Filter messages by regular expressions.</source>
        <translation>Meldungen mit regulären Ausdrücken filtern.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1812"/>
        <source>Print...</source>
        <translation>Drucken...</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1813"/>
        <source>Open the print dialog.</source>
        <translation>Öffnet den Druckdialog.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1815"/>
        <source>Export...</source>
        <translation>Exportieren...</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1816"/>
        <source>Open the export dialog.</source>
        <translation>Öffnet den Dialog zum Exportieren der Daten.</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="390"/>
        <source>Failed to open %1!</source>
        <translation>Öffnen von %1 fehlgeschlagen!</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="400"/>
        <source>Failed to parse XML, line %2, column %3: %4</source>
        <translation>Verarbeiten des XMLs fehlgeschlagen in Zeile %2, Spalte %3: %4</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="490"/>
        <source>Failed to open %1 for writing!</source>
        <translation>Anlegen von %1 fehlgeschlagen!</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1758"/>
        <source>Go to date</source>
        <translation>Gehe zu Datum</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="1676"/>
        <source>Failed to get channel %1: %2</source>
        <translation>Kanal %1 nicht gefunden: %2</translation>
    </message>
    <message>
        <location filename="Graph.cpp" line="2115"/>
        <source>Failed to load section: %1</source>
        <translation>Verarbeiten des Abschnitts fehlgeschlagen: %1</translation>
    </message>
    <message>
        <source>Failed to parse section: %1</source>
        <translation type="obsolete">Verarbeiten des Abschnitts fehlgeschlagen: %1</translation>
    </message>
</context>
<context>
    <name>DLS::SectionDialog</name>
    <message numerus="yes">
        <location filename="SectionDialog.cpp" line="264"/>
        <source>Remove %n layer(s)</source>
        <translation>
            <numerusform>%n Abschnitt entfernen</numerusform>
            <numerusform>%n Abschnitte entfernen</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>DLS::SectionModel</name>
    <message>
        <location filename="SectionModel.cpp" line="184"/>
        <source>Channel</source>
        <translation>Kanal</translation>
    </message>
    <message>
        <location filename="SectionModel.cpp" line="187"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="SectionModel.cpp" line="190"/>
        <source>Unit</source>
        <translation>Einheit</translation>
    </message>
    <message>
        <location filename="SectionModel.cpp" line="193"/>
        <source>Color</source>
        <translation>Farbe</translation>
    </message>
    <message>
        <location filename="SectionModel.cpp" line="196"/>
        <source>Scale</source>
        <translation>Skalierung</translation>
    </message>
    <message>
        <location filename="SectionModel.cpp" line="199"/>
        <source>Offset</source>
        <translation>Versatz</translation>
    </message>
    <message>
        <location filename="SectionModel.cpp" line="202"/>
        <source>Precision</source>
        <translation>Genauigkeit</translation>
    </message>
</context>
<context>
    <name>DatePickerDialog</name>
    <message>
        <location filename="DatePickerDialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Datumsauswahl</translation>
    </message>
    <message>
        <location filename="DatePickerDialog.ui" line="25"/>
        <source>Day</source>
        <translation>Tag</translation>
    </message>
    <message>
        <location filename="DatePickerDialog.ui" line="35"/>
        <source>Week</source>
        <translation>Woche</translation>
    </message>
    <message>
        <location filename="DatePickerDialog.ui" line="42"/>
        <source>Month</source>
        <translation>Monat</translation>
    </message>
    <message>
        <location filename="DatePickerDialog.ui" line="49"/>
        <source>Year</source>
        <translation>Jahr</translation>
    </message>
</context>
<context>
    <name>Dir</name>
    <message>
        <location filename="Dir.cpp" line="108"/>
        <source>Local directory %1</source>
        <translation>Lokales Verzeichnis %1</translation>
    </message>
    <message>
        <location filename="Dir.cpp" line="114"/>
        <source>Remote directory %1</source>
        <translation>Netzwerkverzeichnis %1</translation>
    </message>
</context>
<context>
    <name>ExportDialog</name>
    <message>
        <location filename="ExportDialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Exportdialog</translation>
    </message>
    <message>
        <location filename="ExportDialog.ui" line="28"/>
        <source>Number of Signals:</source>
        <translation>Anzahl Signale:</translation>
    </message>
    <message>
        <location filename="ExportDialog.ui" line="54"/>
        <source>Beginning:</source>
        <translation>Startzeit:</translation>
    </message>
    <message>
        <location filename="ExportDialog.ui" line="74"/>
        <source>End:</source>
        <translation>Endzeit:</translation>
    </message>
    <message>
        <location filename="ExportDialog.ui" line="94"/>
        <source>Duration:</source>
        <translation>Dauer:</translation>
    </message>
    <message>
        <location filename="ExportDialog.ui" line="116"/>
        <source>Target Directory</source>
        <translation>Zielverzeichnis</translation>
    </message>
    <message>
        <location filename="ExportDialog.ui" line="135"/>
        <source>Change...</source>
        <translation>Ändern...</translation>
    </message>
    <message>
        <location filename="ExportDialog.ui" line="145"/>
        <source>Export Formats</source>
        <translation>Exportformate</translation>
    </message>
    <message>
        <location filename="ExportDialog.ui" line="151"/>
        <source>ASCII (*.dat)</source>
        <translation>ASCII (*.dat)</translation>
    </message>
    <message>
        <location filename="ExportDialog.ui" line="161"/>
        <source>Matlab binary, level 4 (*.mat)</source>
        <translation>Matlab-Binärformat, Level 4 (*.mat)</translation>
    </message>
    <message>
        <location filename="ExportDialog.ui" line="173"/>
        <source>Decimation:</source>
        <translation>Untersetzung:</translation>
    </message>
    <message>
        <location filename="ExportDialog.ui" line="190"/>
        <source>Trim data to viewed time range</source>
        <translation>Auf sichtbaren Bereich zuschneiden</translation>
    </message>
    <message>
        <location filename="ExportDialog.ui" line="197"/>
        <source>Make times relative to beginning</source>
        <translation>Relative Zeitstempel</translation>
    </message>
</context>
<context>
    <name>FilterDialog</name>
    <message>
        <location filename="FilterDialog.ui" line="14"/>
        <source>Filter messages</source>
        <translation>Meldungen filtern</translation>
    </message>
    <message>
        <location filename="FilterDialog.ui" line="20"/>
        <source>Regular expression (PCRE syntax):</source>
        <translation>Regulärer Ausdruck (PCRE-Syntax):</translation>
    </message>
</context>
<context>
    <name>Job</name>
    <message>
        <location filename="Job.cpp" line="104"/>
        <source>Job %1</source>
        <translation>Auftrag %1</translation>
    </message>
</context>
<context>
    <name>Layer</name>
    <message>
        <location filename="Layer.cpp" line="104"/>
        <source>Layer element missing url attribute!</source>
        <translation>Kein Attribut url in Layer-Element!</translation>
    </message>
    <message>
        <location filename="Layer.cpp" line="244"/>
        <source>Invalid URL %1!</source>
        <translation>Ungültige URL „%1“!</translation>
    </message>
    <message>
        <location filename="Layer.cpp" line="253"/>
        <source>Failed to get channel %1: %2</source>
        <translation>Kanal „%1“ nicht gefunden: %2</translation>
    </message>
</context>
<context>
    <name>QtDls::Model</name>
    <message>
        <location filename="Model.cpp" line="407"/>
        <source>Path</source>
        <translation>Pfad</translation>
    </message>
    <message>
        <location filename="Model.cpp" line="409"/>
        <source>Alias</source>
        <translation>Alias</translation>
    </message>
</context>
<context>
    <name>Section</name>
    <message>
        <location filename="Section.cpp" line="190"/>
        <source>Invalid value in ScaleMinimum</source>
        <translation>Ungültiger Wert in Attribut ScaleMinimum</translation>
    </message>
    <message>
        <location filename="Section.cpp" line="200"/>
        <source>Invalid value in ScaleMaximum</source>
        <translation>Ungültiger Wert in Attribut ScaleMaximum</translation>
    </message>
    <message>
        <location filename="Section.cpp" line="210"/>
        <source>Invalid value in Height</source>
        <translation>Ungültiger Wert in Attribut Height</translation>
    </message>
    <message>
        <location filename="Section.cpp" line="220"/>
        <source>Invalid value in RelativePrintHeight</source>
        <translation>Ungültiger Wert in Attribut RelativePrintHeight</translation>
    </message>
    <message>
        <source>Layer element missing url attribute!</source>
        <translation type="vanished">Kein Attribut url in Layer-Element!</translation>
    </message>
    <message>
        <source>Invalid URL in Layer element!</source>
        <translation type="vanished">Ungültige URL in Layer-Element!</translation>
    </message>
    <message>
        <source>Failed to get channel %1: %2</source>
        <translation type="vanished">Kanal %1 nicht gefunden: %2</translation>
    </message>
    <message>
        <source>Failed to get channel %1!</source>
        <translation type="vanished">Kanal %1 nicht gefunden!</translation>
    </message>
    <message>
        <location filename="Section.cpp" line="972"/>
        <source>Failed to load layer: %1</source>
        <translation>Schicht konnte nicht geladen werden: %1</translation>
    </message>
</context>
<context>
    <name>SectionDialog</name>
    <message>
        <location filename="SectionDialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>Abschnittseigenschaften</translation>
    </message>
    <message>
        <location filename="SectionDialog.ui" line="22"/>
        <source>Scale</source>
        <translation>Skala</translation>
    </message>
    <message>
        <location filename="SectionDialog.ui" line="28"/>
        <source>Automatic</source>
        <translation>Automatisch</translation>
    </message>
    <message>
        <location filename="SectionDialog.ui" line="35"/>
        <source>Manual</source>
        <translation>Manuell</translation>
    </message>
    <message>
        <location filename="SectionDialog.ui" line="44"/>
        <source>Maximum:</source>
        <translation>Maximum:</translation>
    </message>
    <message>
        <location filename="SectionDialog.ui" line="51"/>
        <source>Minimum:</source>
        <translation>Minimum:</translation>
    </message>
    <message>
        <location filename="SectionDialog.ui" line="80"/>
        <source>Guess from Data</source>
        <translation>Aus Daten erraten</translation>
    </message>
    <message>
        <location filename="SectionDialog.ui" line="87"/>
        <source>Show Scale</source>
        <translation>Skala anzeigen</translation>
    </message>
    <message>
        <location filename="SectionDialog.ui" line="96"/>
        <source>Relative printing
height:</source>
        <translation>Relative Höhe
im Ausdruck:</translation>
    </message>
    <message>
        <location filename="SectionDialog.ui" line="104"/>
        <source>(unused)</source>
        <translation>(keine)</translation>
    </message>
    <message>
        <location filename="SectionDialog.ui" line="107"/>
        <source> %</source>
        <translation> %</translation>
    </message>
    <message>
        <location filename="SectionDialog.ui" line="161"/>
        <source>Preview</source>
        <translation>Vorschau</translation>
    </message>
</context>
</TS>
