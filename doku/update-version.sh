#!/bin/sh
#
# Output information about parent revision to TeX-includable file.
#

unset GIT_DIR

cd doku

REV=$(git describe)
# 1.4.0-rc2-179-gcf2f735
DATE=$(git show -s --format=%cd --date=short)
# 2021-04-14

TEMP="\\def\\revision{$REV}
\\def\\isodate#1-#2-#3x{
  \\day  = #3
  \\month= #2
  \\year = #1
}
\\isodate ${DATE}x
"

OUT=version.tex

echo "$TEMP" > $OUT

echo Updated $OUT
