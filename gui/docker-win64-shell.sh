#!/bin/bash


docker run -it --rm \
    --volume ${PWD}/..:/git \
    --workdir /git \
    --user $(id -u):$(id -g) \
    docker:5000/dls-mingw64-qt-5.15-fedora-35 \
    bash -i
