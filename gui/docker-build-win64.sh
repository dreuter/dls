#!/bin/bash
#compilation of all requisites for dlsgui and of dlsgui itself

MINGW_ROOT=/usr/x86_64-w64-mingw32/sys-root/mingw

#The MINGW_ROOT directory has write access to others.
#This allows for "make install" to all users.
#The right way is to work with --prefix=some local directory,
#but that does not work currently.

#qmake for widget with qt bug which requires the duplicate call for qmake and make install

REV=$(git -C . describe || echo "");

./bootstrap.sh &&\
    mingw64-configure \
        --prefix=$MINGW_ROOT \
        --with-fftw3-dir=$MINGW_ROOT \
        --with-zlib-dir=$MINGW_ROOT \
        --disable-daemon \
        --disable-fltk \
        --disable-tool && \
    make -j4 && \
    make install && \
    cd widgets && \
    x86_64-w64-mingw32-qmake-qt5 \
	PREFIX=$MINGW_ROOT && \
    make -j4 && \
    make install && \
    x86_64-w64-mingw32-qmake-qt5 \
	PREFIX=$MINGW_ROOT && \
    make install && \
    cd ../gui && \
    x86_64-w64-mingw32-qmake-qt5 && \
    make -j4 && \
    make clean && \
    cd release && \
    ../copy-dlls.sh && \
    cd .. && \
    zip -r dlsgui-win64-$REV.zip release

#"make clean" removes the obj in release
