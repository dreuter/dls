#!/bin/sh
#to call this script one must be in the directory where the .exe is
set -e
set -x

echo -e "[Paths] \nPrefix=." > qt.conf

mkdir -p platforms
cp /usr/x86_64-w64-mingw32/sys-root/mingw/lib/qt5/plugins/platforms/* platforms/

mkdir -p imageformats
cp /usr/x86_64-w64-mingw32/sys-root/mingw/lib/qt5/plugins/imageformats/qsvg.dll imageformats/

cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/libbz2-1.dll .
cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/libgcc_s_seh-1.dll .
cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/libfreetype-6.dll .
cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/libglib-2.0-0.dll .
cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/libharfbuzz-0.dll .
cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/libintl-8.dll .
cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/libstdc++-6.dll .
cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/Qt5Core.dll .
cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/Qt5Gui.dll .
cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/Qt5Widgets.dll .
cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/DlsWidgets0.dll .
cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/iconv.dll .
cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/libfftw3-3.dll .
cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/libgcc_s_seh-1.dll .
cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/libharfbuzz-0.dll .
cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/libpcre-1.dll .
cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/libpcre2-16-0.dll .
cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/libpng16-16.dll .
cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/libprotobuf-25.dll .
cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/libssp-0.dll .
cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/libstdc++-6.dll .
cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/liburiparser-1.dll .
cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/libwinpthread-1.dll .
cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/libxml2-2.dll .
cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/Qt5PrintSupport.dll .
cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/Qt5Svg.dll .
cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/Qt5Widgets.dll .
cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/Qt5Xml.dll .
cp /usr/x86_64-w64-mingw32/sys-root/mingw/bin/zlib1.dll .


